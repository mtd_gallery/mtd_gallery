package hu.magictreedecor.mgtserverapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgtServerAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MgtServerAppApplication.class, args);
	}

}
